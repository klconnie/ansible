#!/bin/sh

sysctl -a | grep temperature | awk '
BEGIN {FS=": "}
{
    split($1, parts, ".")
    if (parts[1] == "hw") {
        device = "thermal"
        number = parts[4]
    } else if (parts[1] == "dev") {
        device = "cpu"
        number = parts[3]
    }
    gsub(/C$/, "", $2)
    printf "temperature{device=\"%s\",number=\"%s\"} %s\n", device, number, $2
}'

echo $metrics
