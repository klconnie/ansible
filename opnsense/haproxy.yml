---
- name: Configure HAProxy on opnsense
  hosts: localhost
  connection: local
  vars_files:
    - vars/haproxy.yml
    - vars/dns_dhcp.yml
    - vars/all.yml
    - /etc/ansible/vault.yml
  tasks:
    - name: Configure HAProxy real servers
      ansible.builtin.uri:
        url: "https://{{ firewall_url }}/api/haproxy/settings/setServer/{{ item.uuid }}"
        method: POST
        force_basic_auth: true
        user: "{{ vlt_opnsense.api_key }}"
        password: "{{ vlt_opnsense.api_secret }}"
        body_format: json
        body:
          server:
            enabled: 1
            name: "{{ item.name }}"
            description: "{{ item.descr }}"
            type: "static"
            address: "{{ item.address }}"
            port: "{{ item.port }}"
            mode: "active"
        status_code: 200
      with_items: "{{ haproxy_backend_servers }}"
      register: rs_result

    - name: Check json.result for each api call
      ansible.builtin.fail:
        msg: "Failed to configure HAProxy real servers. {{ item.json.validations }}"
      when: item.json.result != "saved"
      loop: "{{ rs_result.results }}"

    - name: Configure HAProxy backend pools
      ansible.builtin.uri:
        url: "https://{{ firewall_url }}/api/haproxy/settings/setBackend/{{ item.uuid }}"
        method: POST
        force_basic_auth: true
        user: "{{ vlt_opnsense.api_key }}"
        password: "{{ vlt_opnsense.api_secret }}"
        body_format: json
        body:
          backend:
            enabled: 1
            name: "{{ item.name }}"
            description: "{{ item.descr }}"
            mode: "{{ item.mode }}"
            linkedServers: "{{ item.linkedservers }}"
            proxyProtocol: "{{ item.proxyprot | default('') }}"
            forwardedHeader: "{{ item.fwdhdr | default('0') }}"
            forwardedHeaderParameters: "{{ item.fwdhdrparams | default('') }}"
            forwardFor: "{{ item.fwdfor | default('0') }}"
            customOptions: "{{ item.customopts | default('') }}"
        status_code: 200
      with_items: "{{ haproxy_backend_pools }}"
      register: pool_result

    - name: Check json.result for each api call
      ansible.builtin.fail:
        msg: "Failed to configure HAProxy backend pools. {{ item.json.validations }}"
      when: item.json.result != "saved"
      loop: "{{ pool_result.results }}"

    - name: Configure HAProxy conditions
      ansible.builtin.uri:
        url: "https://{{ firewall_url }}/api/haproxy/settings/setAcl/{{ item.uuid }}"
        method: POST
        force_basic_auth: true
        user: "{{ vlt_opnsense.api_key }}"
        password: "{{ vlt_opnsense.api_secret }}"
        body_format: json
        body:
          acl:
            enabled: 1
            name: "{{ item.name }}"
            description: "{{ item.descr }}"
            expression: "{{ item.expression }}"
            negate: "{{ item.negate | default(0) }}"
            hdr: "{{ item.hdr | default('') }}"
        status_code: 200
      with_items: "{{ haproxy_conditions }}"
      register: acl_result

    - name: Check json.result for each api call
      ansible.builtin.fail:
        msg: "Failed to configure HAProxy conditions. {{ item.json.validations }}"
      when: item.json.result != "saved"
      loop: "{{ acl_result.results }}"

    - name: Configure HAProxy rules
      tags: rules
      ansible.builtin.uri:
        url: "https://{{ firewall_url }}/api/haproxy/settings/setAction/{{ item.uuid }}"
        method: POST
        force_basic_auth: true
        user: "{{ vlt_opnsense.api_key }}"
        password: "{{ vlt_opnsense.api_secret }}"
        body_format: json
        body:
          action:
            enabled: 1
            name: "{{ item.name }}"
            description: "{{ item.descr }}"
            testType: "{{ item.testType | default('if') }}"
            linkedAcls: "{{ item.linkedAcls }}"
            operator: "{{ item.operator | default('and') }}"
            type: "{{ item.type }}"
            use_backend: "{{ item.use_backend }}"
        status_code: 200
      with_items: "{{ haproxy_rules }}"
      register: rule_result

    - name: Check json.result for each api call
      ansible.builtin.fail:
        msg: "Failed to configure HAProxy rules. {{ item.json.validations }}"
      when: item.json.result != "saved"
      loop: "{{ rule_result.results }}"

    - name: Configure HAProxy virtual services
      tags: virtual_service
      ansible.builtin.uri:
        url: "https://{{ firewall_url }}/api/haproxy/settings/setFrontend/{{ item.uuid }}"
        method: POST
        force_basic_auth: true
        user: "{{ vlt_opnsense.api_key }}"
        password: "{{ vlt_opnsense.api_secret }}"
        body_format: json
        body:
          frontend:
            enabled: 1
            name: "{{ item.name }}"
            description: "{{ item.descr }}"
            bind: "{{ item.bind }}"
            mode: "{{ item.mode }}"
            sslCert: "{{ item.sslCert }}"
            sslDefaultCert: "{{ item.sslDefaultCert }}"
            ssl_enabled: "{{ item.sslEnabled | default(1) }}"
            linkedPools: "{{ item.linkedPools }}"
            linkedActions: "{{ item.linkedActions }}"
        status_code: 200
      with_items: "{{ haproxy_virtual_service }}"
      register: vs_result

    - name: Check json.result for each api call
      tags: virtual_service
      ansible.builtin.fail:
        msg: "Failed to configure HAProxy virtual services. {{ item.json.validations }}"
      when: item.json.result != "saved"
      loop: "{{ vs_result.results }}"

    - name: Perform HaProxy reconfigre API call to apply changes
      ansible.builtin.uri:
        url: "https://{{ firewall_url }}/api/haproxy/service/reconfigure"
        method: POST
        force_basic_auth: true
        user: "{{ vlt_opnsense.api_key }}"
        password: "{{ vlt_opnsense.api_secret }}"
        status_code: 200

    - name: Debug docker ip
      ansible.builtin.debug:
        msg: "{{ docker_ip }}"
