---
- name: Configure basic settings
  hosts: garagepi
  vars_files:
    - /etc/ansible/vault.yml
  become: true
  tasks:
    - name: Update packages
      ansible.builtin.apt:
        update_cache: true
        upgrade: dist

    - name: Install packages
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
      loop:
        - unattended-upgrades
        - postfix
        - vim
        - screen

    # With help of https://functionofchaos.com/send-server-notifications-to-slack-channel/
    - name: Postfix and unattended upgrades configuration
      tags: unattended-upgrades
      block:
        - name: Add notification dummy user
          ansible.builtin.user:
            name: "notifyuser"
            comment: "Notification user"
            createhome: true
            shell: "/bin/false"
            system: false

        - name: Configure postfix
          ansible.builtin.template:
            src: "templates/etc/postfix/main.cf.j2"
            dest: "/etc/postfix/main.cf"
            owner: root
            group: root
            mode: '0644'
          notify:
            - Restart postfix

        - name: Add entry to aliasses
          ansible.builtin.lineinfile:
            path: "/etc/aliases"
            line: 'notifyuser: "|/home/notifyuser/notifypushover.sh"'
            create: true
            mode: '0644'

        - name: Put notify script in place
          ansible.builtin.template:
            src: "templates/home/notifyuser/notifypushover.sh.j2"
            dest: "/home/notifyuser/notifypushover.sh"
            owner: notifyuser
            group: notifyuser
            mode: '0755'

        - name: Rebuild aliases database
          ansible.builtin.command:
            cmd: newaliases

        - name: Configure unattended-upgrades
          ansible.builtin.template:
            src: "templates/etc/apt/apt.conf.d/52unattended-upgrades-local.j2"
            dest: "/etc/apt/apt.conf.d/52unattended-upgrades-local"
            owner: root
            group: root
            mode: '0644'

    - name: Configure serial port
      ansible.builtin.command: "raspi-config nonint {{ item }}"
      loop:
        - "do_serial_hw 1"
        - "do_serial_cons 0"

    - name: Ensure serial port in /boot/firmware/config.txt is enabled
      ansible.builtin.lineinfile:
        path: "/boot/firmware/config.txt"
        line: "enable_uart=1"
        regexp: "^#?enable_uart=0"
        create: true
        mode: '0755'

  handlers:
    - name: Restart postfix
      ansible.builtin.systemd:
        name: postfix
        state: restarted
